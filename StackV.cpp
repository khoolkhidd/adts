#include "StackV.h"
#include <iostream>
#include <vector>

int Stack::size()
{
	return data.size();
}

void Stack::push(int x)
{
	return data.push_back(x);
}

void Stack::pop()
{
	data.pop_back();
}

int Stack::top()
{
	int a = 0;
	a = data.size() - 1;
	return data[a];
}

void Stack::clear()
{
	while (size() != 0 )
	data.pop_back();
}




